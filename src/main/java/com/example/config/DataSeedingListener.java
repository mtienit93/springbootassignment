package com.example.config;

import java.util.Date;
import java.util.HashSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import com.example.model.Role;
import com.example.model.User;
import com.example.repository.RoleRepository;
import com.example.repository.UserRepository;

@Component
public class DataSeedingListener implements ApplicationListener<ContextRefreshedEvent> {

	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private RoleRepository roleRepository;
	
	@Autowired 
    private PasswordEncoder passwordEncoder;
	
	@Override
	public void onApplicationEvent(ContextRefreshedEvent arg0) {
		
		if (roleRepository.findByName("ROLE_ADMIN") == null) {
            roleRepository.save(new Role("ROLE_ADMIN"));
        }
		
		if (roleRepository.findByName("ROLE_MEMBER") == null) {
            roleRepository.save(new Role("ROLE_MEMBER"));
        }
		
		if (roleRepository.findByName("ROLE_MOD") == null) {
            roleRepository.save(new Role("ROLE_MOD"));
        }
		
		Date date = new Date();
		
		// Admin account
        if (userRepository.findByUserName("admin@gmail.com") == null) {
            User admin = new User();
            admin.setUserName("admin@gmail.com");
            admin.setPassword(passwordEncoder.encode("123456"));
            admin.setStartDate(date);
            HashSet<Role> roles = new HashSet<>();
            roles.add(roleRepository.findByName("ROLE_ADMIN"));
            roles.add(roleRepository.findByName("ROLE_MOD"));
            roles.add(roleRepository.findByName("ROLE_MEMBER"));
            admin.setRoles(roles);
            userRepository.save(admin);
        }
        
     // Mod account
        if (userRepository.findByUserName("mod@gmail.com") == null) {
            User admin = new User();
            admin.setUserName("mod@gmail.com");
            admin.setPassword(passwordEncoder.encode("123456"));
            admin.setStartDate(date);
            HashSet<Role> roles = new HashSet<>();
            roles.add(roleRepository.findByName("ROLE_MOD"));
            roles.add(roleRepository.findByName("ROLE_MEMBER"));
            admin.setRoles(roles);
            userRepository.save(admin);
        }
        
     // member account
        if (userRepository.findByUserName("member@gmail.com") == null) {
            User admin = new User();
            admin.setUserName("member@gmail.com");
            admin.setPassword(passwordEncoder.encode("123456"));
            admin.setStartDate(date);
            HashSet<Role> roles = new HashSet<>();
            roles.add(roleRepository.findByName("ROLE_MEMBER"));
            admin.setRoles(roles);
            userRepository.save(admin);
        }
	}

}
