package com.example.service;

import java.util.Date;

import org.springframework.stereotype.Service;

import com.example.model.User;

@Service
public class UserService {

	public double calLoyalPoint(User user) {
		Date currentDate = new Date();
		return ((currentDate.getTime() - user.getStartDate().getTime() + 1) * 5);
		
	}
}
