package com.example.restcontroller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.model.User;
import com.example.repository.UserRepository;
import com.example.service.UserService;

@RestController
@RequestMapping("/user")
public class UserController {

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private UserService userService;
	
	@GetMapping("/username/{id}")
	public ResponseEntity<String> findUserNameByUserId(@PathVariable String id) {
		Optional<User> user = userRepository.findById(Integer.valueOf(id));
		if(user.isPresent()) {
			return ResponseEntity.ok().body(user.get().getUserName());
		}
		return ResponseEntity.notFound().build();
	}
	
	@GetMapping("/startdate/{id}")
	public ResponseEntity<String> findStartDateByUserId(@PathVariable String id) {
		Optional<User> user = userRepository.findById(Integer.valueOf(id));
		if(user.isPresent()) {
			return ResponseEntity.ok().body(user.get().getStartDate().toString());
		}
		return ResponseEntity.notFound().build();
	}
	
	@GetMapping("/loyalpoint/{id}")
	public ResponseEntity<User> findLoyalPointByUserId(@PathVariable String id) {
		Optional<User> userOptional = userRepository.findById(Integer.valueOf(id));
		if(userOptional.isPresent()) {
			User user = userOptional.get();
			user.setLoyalPoint(userService.calLoyalPoint(user));
			user.setPassword("secret");
			return ResponseEntity.ok().body(user);
		}
		return ResponseEntity.notFound().build();
	}
}
